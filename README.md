# "Random" Seat Assigner

### Steps to be up and randomizing:
1. Clone this repository...
2. Install packages (```npm install```).
3. Replace the names in the file ```src/assets/cadets.json``` with the names of your cadets, keep in mind each inner array should correspond to the number of seats in each row from the front row to the rear. The order in "Jerk Mode" will be the same as in this file!
4. Launch the app (```npm start```).
5. Visit ```localhost:3000```.
6. The application starts in "Random Mode", meaning the order of the seats will be assigned randomly. Press ```S``` to enter "Jerk Mode" and assign the seats by the same order as the file.
7. Zoom in / out to adjust.
8. Press ```space``` to assign a new seat, until all seats are complete.
9. Watch your puppets dance!
